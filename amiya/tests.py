from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import recommendMovie;
# Create your tests here.
class AppTest(TestCase):
    def test_movies_is_exist(self):
        response = Client().get('/recommendMovie/')
        self.assertEqual(response.status_code, 200)

    def test_using_theater(self):
        found = resolve('/recommendMovie/')
        self.assertEqual(found.func, recommendMovie)
        
    def test_using_theater_template(self):
        response = Client().get('/recommendMovie/')
        self.assertTemplateUsed(response, 'recommendMovie.html')

# def setUp(self):
#     self.client = Client()
#     self.recommendMovie = reverse('recommendMovie')

# def test_profile_url_doesnt_exist(self):
#     response = Client().get('/none')
#     self.assertEqual(response.status_code, 404)
    




    