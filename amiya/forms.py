from django import forms
from .models import Movies

# class searchbox (forms.Form):
#     search_query = forms.CharField(min_length=1)

class MoviesForms(forms.ModelForm):
    class Meta:
        model = Movies
        fields = ['title']
        widgets = {
            'title' : forms.Textarea(attrs={'cols':10, 'rows': 5, "class":"form-control"}),

        }
