from django.urls import path
from . import views

app_name = 'amiya'

urlpatterns = [
    path('recommendMovie/', views.recommendMovie, name='recommendMovie'),
    path('webservice/', views.simple_webservice),
]
