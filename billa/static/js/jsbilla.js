const api_key = "feb6f0eeaa0a72662967d77079850353";
let base_img_url = "https://image.tmdb.org/t/p/w300";

function searchMovie(api_url) {

    $('#results').html("");
    $.ajax({
        type: 'GET',
        url: api_url,
        success: function(data) {
            for (let i = 0; i < data.results.length; i++) {
                $('#results').append(
                    "<table border='1'>"+
                    "<tr>" +
                    "<td class='gambar'>" + "<img src='" + base_img_url + data.results[i].poster_path + "'/>" + "</td>" +
                    "<td class='judul' hidden>" + data.results[i].title + "</td>" +
                    "<td class='overview' hidden>" + data.results[i].overview +  "</td>" +
                    "</tr>" +
                    "</table>"
                );
            }
        }
    });
}

$(document).ajaxComplete(function() {
    $('.gambar').click(function() {
        let poster = $(this).closest('tr').children('td.gambar').children('img').attr('src');
        let title = $(this).closest('tr').children('td.judul').text();
        let overview = $(this).closest('tr').children('td.overview').text();
        $('.fotoP').attr('src', poster);
        $('.judulP').text(title);
        $('.paragraphP').text(overview);
        $('.modal').modal();
    });
});


$("#NPbutton").click(function() {
    let url = "https://api.themoviedb.org/3/movie/now_playing?api_key=feb6f0eeaa0a72662967d77079850353&language=en-US&page=1";
    searchMovie(url);
});

$("#TRbutton").click(function() {
    let url = "https://api.themoviedb.org/3/movie/top_rated?api_key=feb6f0eeaa0a72662967d77079850353&language=en-US&page=1";
    searchMovie(url);
});

$("#Pbutton").click(function() {
    let url = "https://api.themoviedb.org/3/movie/popular?api_key=feb6f0eeaa0a72662967d77079850353&language=en-US&page=1";
    searchMovie(url);
});

$("#UPbutton").click(function() {
    let url = "https://api.themoviedb.org/3/movie/upcoming?api_key=feb6f0eeaa0a72662967d77079850353&language=en-US&page=1";
    searchMovie(url);
});


$("#isi").keyup(function() {
    var q = $(this).val().toLowerCase()
    console.log(q)
    let url = "https://api.themoviedb.org/3/search/movie?&api_key=feb6f0eeaa0a72662967d77079850353&language=en-US&query="+q+"&page=1&include_adult=false";
    searchMovie(url);
});
