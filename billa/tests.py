from django.test import TestCase,Client
from django.urls import resolve
from . import views
from django.http import HttpRequest


class SearchTest(TestCase):
    def test_movies_is_exist(self):
        response = Client().get('/searchMovie/')
        self.assertEqual(response.status_code, 200)
    
    # def test_using_searchMovie(self):
    #     found = resolve('/searchMovie/')
    #     self.assertEqual(found.func,searchMovie)
    
    def test_chef_using_kurir_func(self):
        found = resolve('/searchMovie/')
        self.assertEqual(found.func, views.searchMovie)
    
    def test_feedback_is_exist(self):
        response = Client().get('/SearchMovie/')
        self.assertEqual(response.status_code, 404)
        
    def test_using_searchMovie_template(self):
        response = Client().get('/searchMovie/')
        self.assertTemplateUsed(response, 'searchMovie.html')
    
    # def test_movies_has_title(self):
    #     response = Client().get('/searchMovie/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn("Let's Find a Movie!", html_response)


