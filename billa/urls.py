from django.urls import path
from . import views

app_name = 'billa'

urlpatterns = [
    path('searchMovie/', views.searchMovie, name='searchMovie'),
    path('webservices/', views.simple_webservices),
]