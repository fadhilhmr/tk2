from django.shortcuts import render
from .models import Movies
from django.http import JsonResponse

def review(request):
    movies = None

    if 'search' in request.GET.keys() :
        keyword = request.GET['search'] 
        movies = Movies.objects.filter(title = keyword)

    return render(request, 'age.html',{ 'Movies' : movies})


def simple_webservice(request):
    data_dikirim = {'data_1': 'abc', 'data_2': 'def', 'data_3': 'ghi'}
    return JsonResponse(data=data_dikirim)
