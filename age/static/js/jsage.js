const api_key = "feb6f0eeaa0a72662967d77079850353";
let base_img_url = "https://image.tmdb.org/t/p/w300";

function nowPlaying(api_url) {

    $('#results').html("");
    $.ajax({
        type: 'GET',
        url: api_url,
        success: function(data) {
            for (let i = 0; i < data.results.length; i++) {
                $('#results').append(
                    "<table class='col-md-3 col-sm-12' border='1'"+
                    "<tr data-type=" + data.results[i].id +">" +
                    "<td href=# class='gambar'>" + "<img src='" + base_img_url + data.results[i].poster_path + "'/>" + "</td>" +
                    "<td class='judul' hidden><b>" + data.results[i].title + "</b></td>" +
                    "<td class='overview' hidden>" + data.results[i].overview +  "</td>" +
                    "<div id='pop-up'><p>"+ data.results[i].title +"</p>"+
                    "</tr>" +
                    "</table>"
                );
            }
        }
    });
}

$(document).ajaxComplete(function() {
    var moveLeft = 20;
    var moveDown = 10;
    $('.gambar').hover(function(e) {
        $('div#pop-up').show();
        //.css('top', e.pageY + moveDown)
        //.css('left', e.pageX + moveLeft)
        //.appendTo('body');
    }, function() {
        $('div#pop-up').hide();
    });
    $('a.trigger').mousemove(function(e) {
        $("div#pop-up").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
    });


    $('.gambar').click(function() {
        let poster = $(this).closest('tr').children('td.gambar').children('img').attr('src');
        let title = $(this).closest('tr').children('td.judul').text();
        let overview = $(this).closest('tr').children('td.overview').text();
        $('.fotoP').attr('src', poster);
        $('.judulP').text(title);
        $('.paragraphP').text(overview);
        $('.modal').modal();
    });
});


$("#NPbutton").click(function() {
    let url =  "https://api.themoviedb.org/3/movie/upcoming?api_key=feb6f0eeaa0a72662967d77079850353&language=en-US&page=1";
    nowPlaying(url);
});

$("#NPbutton").hover(function(){
    $(this).css("background-color", "#FF786A");
    }, function(){
    $(this).css("background-color", "gray");
  });




