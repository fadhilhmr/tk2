from django.contrib import admin
from django.urls import path
from .views import review, simple_webservice

#app_name = 'age'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('review/', review, name='review'),
    path('webservice/', simple_webservice)
]
