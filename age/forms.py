from django import forms
from .models import Review

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['review_title', 'review']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class' : 'form-control'
            })