from django.test import TestCase
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings
from .views import *

# Create your tests here.

class Story9PPWUnitTest(TestCase):
    def test_login_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_login_url_doesnt_exist(self):
        response = Client().get('/none')
        self.assertEqual(response.status_code, 404)

    def test_login_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_login_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_welcome_page_after_login(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/')
        self.assertEqual(response.status_code,200)

    def test_welcome_page_after_login_url_doesnt_exist(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/apaya')
        self.assertEqual(response.status_code,404)

    def test_welcome_using_index_template(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing.html')

    # def test_welcome_page_after_redirect(self):
    #     user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
    #     self.client.login(username='john', password='johnpassword')
    #     response = self.client.get('')
    #     self.assertEqual(response.status_code,302)

    # def test_logout_redirect(self):
    #     user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
    #     self.client.login(username='john', password='johnpassword')
    #     response = self.client.post('')
    #     self.assertEqual(response.status_code,302)
