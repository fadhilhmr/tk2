from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from . import views
from fadhil.views import library, simple_webservice
from .models import Movies
from django.http import HttpRequest

# Create your tests here.

class testCase(TestCase):
    def test_library_is_exist(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code, 200)

    def test_using_library(self):
        found = resolve('/library/')
        self.assertEqual(found.func, library)
        
    def test_using_library_template(self):
        response = Client().get('/library/')
        self.assertTemplateUsed(response, 'library.html')

    def test_func(self):
            found = resolve('/library/')
            self.assertEqual(found.func, views.library)
            
    def test_using_perpus(self):
        found = resolve('/library/')
        self.assertEqual(found.func, library)

    def test_story6_button_available(self):
            response = self.client.get('/library/')
            self.assertContains(response, '<button')









