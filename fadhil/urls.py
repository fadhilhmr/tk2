from django.contrib import admin
from django.urls import path
from .views import library, simple_webservice

app_name = 'fadhil'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('library/', library, name =  'library'),
    path('webservice/', simple_webservice),
]
